# wordpress-swarm

Stack para deployar Wordpress en Docker Swarm


# STEPs

## Create required directories to persist data
mkdir -p /data/{wp,mysql,letsencrypt}_data

## Create docker networks
docker network create --driver overlay --scope swarm nw-web

docker network create --driver overlay --scope swarm nw-backend

## Deploy stack
docker stack deploy -c docker-stack.yml wp

# Scaling

You can scale the wordpress and nginx part very easily:

# To scale up:
docker service scale my_wordpress_nginx=3

docker service scale my_wordpress_wordpress=3

# Check whether all replicas are running
docker service ls

# To scale down:
docker service scale my_wordpress_nginx=1

docker service scale my_wordpress_wordpress=1
